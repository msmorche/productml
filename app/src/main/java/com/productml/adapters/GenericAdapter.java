package com.productml.adapters;

import android.content.Context;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * Adaptador genérico
 */
public class GenericAdapter<T, E extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<E>
{
    protected List<T> items;
    protected Context context;

    public GenericAdapter(Context context, List<T> items)
    {
        this.context=context;
        this.items = items;
    }

    public T removeItem(int position)
    {
        final T model = items.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItem(int position, T model) {
        items.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final T model = items.remove(fromPosition);
        items.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    private void applyAndAnimateRemovals(List<T> newModels)
    {
        for (int i = items.size() - 1; i >= 0; i--)
        {
            final T model = items.get(i);
            if (!newModels.contains(model)) {
                removeItem(i);
            }
        }
    }
    private void applyAndAnimateAdditions(List<T> newModels)
    {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final T model = newModels.get(i);
            if (!items.contains(model)) {
                addItem(i, model);
            }
        }
    }

    private void applyAndAnimateMovedItems(List<T> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final T model = newModels.get(toPosition);
            final int fromPosition = items.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void animateTo(List<T> models)
    {
        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models);
        applyAndAnimateMovedItems(models);
    }

    @Override
    public E onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(E holder, int position) {
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public List<T> getItems(){
        return items;
    }

    public void setItems(List<T> items){
        this.items=items;
        notifyDataSetChanged();
    }

    public void addAllItems(List<T> additems){
        this.items.addAll(additems);
        notifyDataSetChanged();
    }
}
