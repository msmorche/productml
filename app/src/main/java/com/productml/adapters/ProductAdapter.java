package com.productml.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.productml.R;
import com.productml.helper.Functions;
import com.productml.models.Product;
import com.productml.viewholder.ProductViewHolder;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

/**
 * Adaptador para lista de productos
 */
public class ProductAdapter extends GenericAdapter<Product, ProductViewHolder> {

    //Interface para usar en el activity
    public interface OnClickListener{
        //Función para mostrar información de un producto
        void showMoreInfo(Product product);
    }

    //Objeto para la interface
    private OnClickListener listener;

    public ProductAdapter(Context context, List<Product> items) {
        super(context,items);
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);

        //Instanciar la interface con el context
        listener=(OnClickListener) parent.getContext();

        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //Obtener el producto según la posición
        Product product=items.get(position);

        //Cargar la imagen thumbnail usando Glide
        Glide.with(context)
                .load(product.getThumbnail())
                .into(holder.card_image);

        //Solo se verifica si es con envío gratis o no
        String shipping=product.getShipping().isFree_shipping()?context.getString(R.string.product_detail_shipping):"";


        //Cargar los valores
        holder.card_title.setText(product.getTitle());
        holder.card_shipping.setText(shipping);
        holder.card_price.setText(Functions.getCurrency(product.getPrice(),product.getCurrency_id()));

        //Guardar el producto como tag para luego usarlo al clickear el items
        holder.itemView.setTag(product);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Mostrar mas información del producto
                listener.showMoreInfo((Product)v.getTag());
            }
        });
    }

}
