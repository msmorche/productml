package com.productml.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.productml.R;
import com.productml.helper.AppController;
import com.productml.helper.Constants;
import com.productml.helper.Functions;
import com.productml.models.Product;

import org.json.JSONObject;

/**
 * Clase bottomsheetdialogfragment para mostrar la inforamción de un producto
 */
public class BottomInfoProductFragment extends BottomSheetDialogFragment{

    //Para logs
    private static final String TAG="InfoProduct";

    //Obteto producto
    private Product product;

    //Views
    private TableLayout table_info;
    private TextView descripcion;

    public BottomInfoProductFragment() {

    }

    //Función para que el bottomsheet se muestre en fullscreen
    @NonNull @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;

                FrameLayout bottomSheet = (FrameLayout) d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        //Obtener el producto desde el bundle
        this.product=bundle.getParcelable(Constants.PRODUCT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_bottom_info_product, container, false);

        //Acción para cerrar el dialog con el icono
        view.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        //Obtener los views
        TextView estado=view.findViewById(R.id.estado);
        TextView title=view.findViewById(R.id.title);
        ImageView image=view.findViewById(R.id.image);
        TextView price=view.findViewById(R.id.price);
        TextView shipping=view.findViewById(R.id.shipping);
        TextView stock=view.findViewById(R.id.stock);
        TextView seller=view.findViewById(R.id.seller);
        TextView cantdad_disponible=view.findViewById(R.id.cantdad_disponible);
        table_info=view.findViewById(R.id.table_info);
        descripcion=view.findViewById(R.id.descripcion);

        //Cargar valores
        //Cargar la imagen thumbnail usando Glide
        Glide.with(getContext())
                .load(product.getThumbnail())
                .into(image);

        StringBuilder txt_estado=new StringBuilder();
        txt_estado
                .append(product.getCondition())
                .append(" | ")
                .append(product.getSold_quantity())
                .append(" ")
                .append(getContext().getString(R.string.bottomsheet_info_product_txt_sold));
        estado.setText(txt_estado);
        title.setText(product.getTitle());
        price.setText(Functions.getCurrency(product.getPrice(),product.getCurrency_id()));

        //Solo se verifica si es con envío gratis o no
        String txt_shipping=product.getShipping().isFree_shipping()?getContext().getString(R.string.product_detail_shipping):"";
        shipping.setText(txt_shipping);

        //Cantidad disponible
        cantdad_disponible.setText(String.valueOf(product.getAvailable_quantity()));

        //Si la cantidad disponible es distinta de 0 hay stock sino no
        String txt_stock=product.getAvailable_quantity()==0?getContext().getString(R.string.bottomsheet_info_product_txt_stock_no):getContext().getString(R.string.bottomsheet_info_product_txt_stock_ok);
        stock.setText(txt_stock);

        //Obtener el nombre de usuario vendedor
        seller.setText(Functions.getUserNameProfile(product.getSeller().getPermalink()));
        seller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(product.getSeller().getPermalink())));
            }
        });

        completarDetalles();

        return view;
    }

    //Obtener los detalles de producto
    public void completarDetalles(){
        //Armar url para la consulta
        String url = String.format(Constants.ML_URL_ITEM,product.getId());
        Log.e(TAG,"url="+url);

        //Verificar conectividad
        if (Functions.isOnline(getContext())) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //Se usa GSON para convertir los datos del json a objetos
                                Product productsaux = new Gson().fromJson(response.toString(), Product.class);

                                //Cargar informacion del producto
                                Functions.loadTableLayout(getContext(),table_info,productsaux.getAttributes());

                                //Luego se completa la descripcion
                                completarDescripcion();

                            } catch (Exception e) {
                                Toast.makeText(getContext(), R.string.msj_error_generic, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, "Error al obtener los campos del item");
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(), R.string.msj_error_generic, Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "Error al enviar la consulta del item");
                            error.printStackTrace();
                        }
                    });

            // Access the RequestQueue through your singleton class.
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
        else
            Toast.makeText(getContext(), R.string.msj_error_conection, Toast.LENGTH_LONG).show();
    }


    //Obtener la descripcion del producto
    public void completarDescripcion(){
        //Armar url para la consulta
        String url = String.format(Constants.ML_URL_ITEM_DESCRIPTION,product.getId());
        Log.e(TAG,"url="+url);

        //Verificar conectividad
        if (Functions.isOnline(getContext())) {
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                descripcion.setText(response.getString("plain_text"));
                            } catch (Exception e) {
                                Toast.makeText(getContext(), R.string.msj_error_generic, Toast.LENGTH_SHORT).show();
                                Log.e(TAG, "Error al obtener los campos del item");
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getContext(), R.string.msj_error_generic, Toast.LENGTH_SHORT).show();
                            Log.e(TAG, "Error al enviar la consulta del item");
                            error.printStackTrace();
                        }
                    });

            // Access the RequestQueue through your singleton class.
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
        else
            Toast.makeText(getContext(), R.string.msj_error_conection, Toast.LENGTH_LONG).show();
    }
}