package com.productml.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.productml.R;
import com.productml.adapters.ProductAdapter;
import com.productml.fragments.BottomInfoProductFragment;
import com.productml.helper.AppController;
import com.productml.helper.Constants;
import com.productml.helper.Functions;
import com.productml.models.Product;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * Pantalla principal de la aplicación
 */
public class MainActivity extends AppCompatActivity implements
        SearchView.OnQueryTextListener,
        ProductAdapter.OnClickListener{

    //Variable estatica para mostrar logs
    public static final String TAG = AppController.class.getSimpleName();

    //Utilizado para controlar la búsqueda
    private SearchView searchView;
    private MenuItem itemsearch;
    private String query;
    private int offset;

    //Views
    private CoordinatorLayout v_principal;
    private LinearLayout li_incial;
    private TextView tv_title;
    private TextView tv_subtitle;
    private RecyclerView recyclerView;
    private TextView results;

    //Linear Layout Manager para recyclerview
    private LinearLayoutManager linearLayoutManager;

    //Objeto usado para mantener la lista de productos
    private ArrayList<Product> products;

    //Objeto usado para mostrar un progreso de busqueda
    private ProgressBar progress_circular;

    //Objeto adaptador
    private ProductAdapter productAdapter;

    //Total de item en el backend
    private long totalitemsbackend;

    //Para indicar si se estan cargando items
    private boolean loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Obtener los views
        v_principal=findViewById(R.id.v_principal);
        li_incial=findViewById(R.id.li_incial);
        tv_title=findViewById(R.id.tv_title);
        tv_subtitle=findViewById(R.id.tv_subtitle);
        recyclerView=findViewById(R.id.recyclerView);
        progress_circular=findViewById(R.id.progress_circular);
        results=findViewById(R.id.results);

        //Inicializar variables
        products=new ArrayList<>();
        query="";
        offset=0;
        productAdapter=new ProductAdapter(this,products);
        totalitemsbackend=0;
        loading=false;

        //Se define el recyclerview de forma lineal
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(productAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //Obtener cantidad de items de la lista
                int totalItemCount =productAdapter.getItemCount();
                //Posición del último view visible
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                //Verificación para solicitar mas items que
                //Si no se mostraron todos los items a solicitar (totalitemsbackend!=totalItemCount)
                //No debe estar cargando la lista (!loading)
                //El total de items de la lista actual debe ser mayor o igual al limite para buscar (totalItemCount >= Constants.ML_URL_LIMIT)
                //El último item visible debe ser el anteúltimo de la lista ((totalItemCount - 1) <= lastVisibleItem))
                if (totalitemsbackend!=totalItemCount && !loading && totalItemCount >= Constants.ML_URL_LIMIT && ((totalItemCount - 1) <= lastVisibleItem)) {
                    loading=true;
                    searchProduct();
                }
                else
                    progress_circular.setVisibility(GONE);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mainactivity, menu);

        itemsearch = menu.findItem(R.id.action_search);

        searchView = (SearchView) itemsearch.getActionView();
        if (searchView != null) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setQueryHint(getString(R.string.menu_search_hint));
            searchView.setOnSearchClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            searchView.setOnQueryTextListener(this);

            //Style
            EditText txtSearch = ((EditText)searchView.findViewById(androidx.appcompat.R.id.search_src_text));
            txtSearch.setTextColor(Color.BLACK);
            txtSearch.setHintTextColor(Color.GRAY);
            ((ImageView)searchView.findViewById(androidx.appcompat.R.id.search_close_btn)).setColorFilter(Color.BLACK);
            ((ImageView)searchView.findViewById(androidx.appcompat.R.id.search_mag_icon)).setColorFilter(Color.BLACK);

        }
        return true;
    }

    //Función para cambiar los mensajes o estados de la pantalla
    public void changeViews(int option, String msj){
        switch (option){
            case 0: //se muestra como en el inicio
                recyclerView.setVisibility(View.GONE);
                li_incial.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.VISIBLE);
                tv_subtitle.setText(getString(R.string.msj_intro));
                results.setText("");
                break;
            case 1: //se oculta todo
                recyclerView.setVisibility(View.VISIBLE);
                li_incial.setVisibility(View.GONE);
                break;
            case 2://se muestra un msj: Ej: no se encontraron resultados
                recyclerView.setVisibility(View.GONE);
                li_incial.setVisibility(View.VISIBLE);
                tv_title.setVisibility(View.GONE);
                tv_subtitle.setText(msj);
                results.setText("");
                break;
        }

        progress_circular.setVisibility(View.GONE);
    }

    @Override
    public boolean onQueryTextSubmit(String txt) {
        //Se busca solo si se ingreso un texto distinto
        if(!txt.equals(query)) {
            //try/catch por si ocurre algún error en el armado de la query
            try {
                this.query = URLEncoder.encode(txt, "UTF-8");
                searchProduct();
            } catch (Exception e) {
                Snackbar.make(v_principal, R.string.msj_error_generic, Snackbar.LENGTH_SHORT).show();
                Log.e(TAG, "Error al armar la query");
                e.printStackTrace();
            }
        }

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //Volver a 0 el offset para una nueva consulta
        if(offset!=0){
            products.clear();
            offset = 0;
            //Se deja la pantalla como en el inicio
            changeViews(0,"");
        }
        return false;
    }

    //Funciónn para obtener los datos
    public void searchProduct(){
        //Armar url para la consulta
        String url = String.format(Constants.ML_URL_SEARCH,offset,query);
        Log.e(TAG,"url="+url);

        //Verificar conectividad
        if (Functions.isOnline(this)) {

            //Mostrar el progress circular
            progress_circular.setVisibility(View.VISIBLE);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                    (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //Guardar el total de items en el backend
                                totalitemsbackend=response.getJSONObject("paging").getInt("total");
                                //El offset para la proxima consulta es el offset actual + el limite
                                offset += response.getJSONObject("paging").getInt("limit");

                                //TipoArrayList para los resultados
                                Type productListType = new TypeToken<ArrayList<Product>>() {}.getType();
                                //Se usa GSON para convertir los datos del json a objetos
                                ArrayList<Product> productsaux = new Gson().fromJson(response.getString("results"), productListType);

                                refreshProducts(productsaux);
                                loading=false;

                            } catch (Exception e) {
                                Snackbar.make(v_principal, R.string.msj_error_generic, Snackbar.LENGTH_SHORT).show();
                                Log.e(TAG, "Error al obtener los campos de la consulta");
                                e.printStackTrace();
                                progress_circular.setVisibility(View.GONE);
                                loading=false;
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Snackbar.make(v_principal, R.string.msj_error_generic, Snackbar.LENGTH_SHORT).show();
                            Log.e(TAG, "Error al enviar la consultar");
                            error.printStackTrace();

                            progress_circular.setVisibility(View.GONE);
                            loading=false;
                        }
                    });

            // Access the RequestQueue through your singleton class.
            AppController.getInstance().addToRequestQueue(jsonObjectRequest);
        }
        else {
            Snackbar.make(v_principal, R.string.msj_error_conection, Snackbar.LENGTH_LONG).show();
            loading=false;
        }

    }

    //Función para listar y actualizar los productos
    public void refreshProducts(ArrayList<Product> newproducts){
        //Mostrar total de items
        String resultados=totalitemsbackend+" "+getString(R.string.title_bar_result);
        results.setText(resultados);

        //Si el offset es 0, es la 1era consulta y reemplazo los productos
        //sino, agrego a los existentes
        if(offset==0)
            products=newproducts;
        else
            products.addAll(newproducts);

        //Actualizar lista
        productAdapter.notifyDataSetChanged();

        //Si no hay productos se muestra un mensaje al respecto
        if(products.size()==0)
            changeViews(2,getString(R.string.msj_no_list));
        else
            changeViews(1,"");
    }

    //Función para mostrar información de un producto
    @Override
    public void showMoreInfo(Product product){
        Bundle bundle=new Bundle();
        bundle.putParcelable(Constants.PRODUCT,product);
        BottomInfoProductFragment bottomInfoProductFragment = new BottomInfoProductFragment();
        bottomInfoProductFragment.setArguments(bundle);
        bottomInfoProductFragment.show(getSupportFragmentManager(), bottomInfoProductFragment.getTag());
    }
}