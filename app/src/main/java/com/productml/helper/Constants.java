package com.productml.helper;

/**
 * Clase para definir constantes
 */
public class Constants {

   //Límite de busqueda
   public final static int ML_URL_LIMIT=20;

   //Url base para realizar la busqueda de productos con limite establecido a 20
   public final static String ML_URL_SEARCH="https://api.mercadolibre.com/sites/MLA/search?limit="+ML_URL_LIMIT+"&offset=%s&q=%s";

   //Url detalle de un item
   public final static String ML_URL_ITEM="https://api.mercadolibre.com/items/%s";

   //Url descripcion de un item
   public final static String ML_URL_ITEM_DESCRIPTION="https://api.mercadolibre.com/items/%s/description";

   //Url del perfil del vendedor
   public final static String ML_URL_PROFILE="http://perfil.mercadolibre.com.ar/";

   //Nombre para representar al obeto product
   public final static String PRODUCT="producto";
}
