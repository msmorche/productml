package com.productml.helper;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.productml.models.Attributes;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;

/**
 * Clase para definir funciones genericas usadas en toda la app
 */
public class Functions {

    //Variable estatica para mostrar logs
    public static final String TAG = AppController.class.getSimpleName();

    //Función para chequear conectividad
    public static boolean isOnline(Context context) {
        //variable de retorno
        boolean ret=false;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager!=null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            ret=networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();
        }

        return ret;
    }

    //Obtener el formato de precio
    public static String getCurrency(double price, String currency_id){
        //Si hay un error, se retorna el precio sin formato
        String retorno=String.valueOf(price);

        NumberFormat format = NumberFormat.getCurrencyInstance();
        try {
            format.setMaximumFractionDigits(0);
            format.setCurrency(Currency.getInstance(currency_id));
            retorno=format.format(price);
        }
        catch (Exception e){
            Log.e(TAG,"Error al obtener el formato de precio");
            e.printStackTrace();
        }

        return retorno;
    }

    //Cargar en un tablelayout 2 columnas de name y value_name
    public static void loadTableLayout(Context context, TableLayout tableLayout, List<Attributes> attributes){
        TableRow.LayoutParams param = null;
        TableRow tr = null;

        for(Attributes attributes1: attributes) {
            tr = new TableRow(context);
            param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, .1f);
            addtxt(context, tr, attributes1.getName(),  param,  Typeface.BOLD);

            param = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT, .1f);
            addtxt(context, tr, attributes1.getValue_name(),  param,  Typeface.NORMAL);

            tableLayout.addView(tr, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    //Agregar textview a un tablerow
    private static void addtxt(Context context,TableRow tr, String str, TableRow.LayoutParams param, int typeface){
        TextView txt = new TextView(context);
        txt.setText(str);
        txt.setTextSize(14f);
        txt.setLayoutParams(param);
        txt.setTypeface(null, typeface);
        tr.addView(txt);
    }

    //Obtener el nombre de usuario vendedor desde el link
    public static String getUserNameProfile(String url){
        String retorno="";
        if(url!=null)
            retorno= url.substring(url.indexOf(Constants.ML_URL_PROFILE)+Constants.ML_URL_PROFILE.length());

        return  retorno;
    }
}
