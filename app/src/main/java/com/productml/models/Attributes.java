package com.productml.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Clase que representa los atributos del producto
 */
public class Attributes implements Parcelable {

    private String id;
    private String name;
    private String value_name;

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Attributes> CREATOR= new Creator<Attributes>() {
        public Attributes createFromParcel(Parcel in) {
            return new Attributes(in);
        }

        public Attributes[] newArray(int size) {
            return new Attributes[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(value_name);
    }

    private Attributes(Parcel in) {
        id=in.readString();
        name=in.readString();
        value_name=in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue_name() {
        return value_name;
    }

    public void setValue_name(String value_name) {
        this.value_name = value_name;
    }
}
