package com.productml.models;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * Clase que representa el vendedor del producto
 */
public class Seller implements Parcelable {

    private String id;
    private String permalink;

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Seller> CREATOR= new Creator<Seller>() {
        public Seller createFromParcel(Parcel in) {
            return new Seller(in);
        }

        public Seller[] newArray(int size) {
            return new Seller[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(permalink);
    }

    private Seller(Parcel in) {
        id=in.readString();
        permalink=in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }
}
