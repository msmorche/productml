package com.productml.models;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

/**
 * Clase que represetena los datos principales de un producto
 */

public class Product implements Parcelable {

    private String id;
    private String title;
    private double price;
    private String currency_id;
    private int available_quantity;
    private int sold_quantity;
    private String condition;
    private String thumbnail;
    private Seller seller;
    private Shipping shipping;
    private List<Attributes> attributes;

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Product> CREATOR= new Parcelable.Creator<Product>() {
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeDouble(price);
        parcel.writeString(currency_id);
        parcel.writeInt(available_quantity);
        parcel.writeInt(sold_quantity);
        parcel.writeString(condition);
        parcel.writeString(thumbnail);
        parcel.writeParcelable(seller,flags);
        parcel.writeParcelable(shipping,flags);
        parcel.writeList(attributes);
    }

    private Product(Parcel in) {
        id=in.readString();
        title=in.readString();
        price=in.readDouble();
        currency_id=in.readString();
        available_quantity=in.readInt();
        sold_quantity=in.readInt();
        condition=in.readString();
        thumbnail=in.readString();
        seller=in.readParcelable(Seller.class.getClassLoader());
        shipping=in.readParcelable(Shipping.class.getClassLoader());
        in.readList(attributes,Attributes.class.getClassLoader());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(String currency_id) {
        this.currency_id = currency_id;
    }

    public int getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(int available_quantity) {
        this.available_quantity = available_quantity;
    }

    public int getSold_quantity() {
        return sold_quantity;
    }

    public void setSold_quantity(int sold_quantity) {
        this.sold_quantity = sold_quantity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public void setShipping(Shipping shipping) {
        this.shipping = shipping;
    }

    public List<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attributes> attributes) {
        this.attributes = attributes;
    }

    public Seller getSeller() {
        return seller;
    }

    public void setSeller(Seller seller) {
        this.seller = seller;
    }
}
