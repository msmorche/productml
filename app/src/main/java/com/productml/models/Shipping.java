package com.productml.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Clase que representa el envío de un producto
 */
public class Shipping implements Parcelable {

    private boolean free_shipping;
    private String mode;
    private ArrayList<String> tags;
    private String logistic_type;
    private String store_pick_up;

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Shipping> CREATOR= new Parcelable.Creator<Shipping>() {
        public Shipping createFromParcel(Parcel in) {
            return new Shipping(in);
        }

        public Shipping[] newArray(int size) {
            return new Shipping[size];
        }
    };

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte)(free_shipping?1:0));
        parcel.writeString(mode);
        parcel.writeList(tags);
        parcel.writeString(logistic_type);
        parcel.writeString(store_pick_up);
    }

    private Shipping(Parcel in) {
        free_shipping=in.readByte()==1;
        mode=in.readString();
        in.readList(tags,getClass().getClassLoader());
        logistic_type=in.readString();
        store_pick_up=in.readString();
    }

    public boolean isFree_shipping() {
        return free_shipping;
    }

    public void setFree_shipping(boolean free_shipping) {
        this.free_shipping = free_shipping;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public ArrayList<String> getTags() {
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public String getLogistic_type() {
        return logistic_type;
    }

    public void setLogistic_type(String logistic_type) {
        this.logistic_type = logistic_type;
    }

    public String getStore_pick_up() {
        return store_pick_up;
    }

    public void setStore_pick_up(String store_pick_up) {
        this.store_pick_up = store_pick_up;
    }
}
