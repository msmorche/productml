package com.productml.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.productml.R;


/**
 * Clase para asociar views con los datos
 */
public class ProductViewHolder extends RecyclerView.ViewHolder {

    public TextView card_title,card_price,card_shipping;
    public ImageView card_image;

    public ProductViewHolder(View v) {
        super(v);
        card_title=v.findViewById(R.id.card_title);
        card_price=v.findViewById(R.id.card_price);
        card_shipping=v.findViewById(R.id.card_shipping);
        card_image=v.findViewById(R.id.card_image);
    }
}
